# NBN23 - Technical Aptitude Test

## Goal
The goal of this test was to create an HTTP API server with a route to be able to query data from 3rd parties in a standard format.

## Implementation
To archive this goal, I created an `expressJs` API with just an endpoint.

To launch the server I've built a `Docker` image handled by `Docker Compose` so the only thing needed (Aside from having those two installed) is:
```
docker-compose up --build -d
```

With this you will have an endpoint listening on port `3000`, so the default route will be `localhost:3000/api/match`

This route also needs (Non optionals) two query parameters:
- *id*: Id of the match you want to get
- *platform*: 3rd party already integrated in the platform where you want to get the data from. [ `foonation`, `barbicle` ]

So an example final route could be:
```
localhost:3000/api/match?id=1&platform=foonation
```

If one of the two parameters is missing it will return a `400` response.


## Testing
For testing purposes the default script `npm test` it's available.

As a testing library I've used `Mocha` with `Chai` and using `Supertest` to do the actual API calls.
Also, If you launch the application without setting the `NODE_ENV` variable to `production`, all the routes will be mocked with `nock`, so no actual call will be made to any platform.

As for the tests I had chosen to do, I'm just testing that all the responses have the expected structure (But without limiting the max amount of properties of the response) to make this tests extensible in a future.

Also quickly testing that the validator for those two properties works.

## Personal notes
It's the first time I'm using `nock` but I enjoyed working with it. Before I was using other libraries more tied to `axios` to mock the responses so I'm gonna keep using this one for sure.

Also I'm really rusty with functional programming so in some places I did it in a way I'm more comfortable with. But will try to recover the lost time with `Ramda`