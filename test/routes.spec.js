const expect = require('chai').expect
const app = require('../app/server')
const request = require('supertest')

describe('--- Router ---', () => {
  it('Should have the API online', done => {
    request(app)
      .get('/api/match')
      .query({ id: 20, platform: 'foonation' })
      .expect('Content-Type', /json/)
      .expect(200, done)
  })

  it('Should return results from Barbicle', done => {
    request(app)
      .get('/api/match')
      .query({ id: 20, platform: 'barbicle' })
      .expect('Content-Type', /json/)
      .expect(200)
      .end((err, res) => {
        expect(res.body.platform).to.equal('barbicle')
        done()
      })
  })

  it('Should return results from Foonation', done => {
    request(app)
      .get('/api/match')
      .query({ id: 20, platform: 'foonation' })
      .expect('Content-Type', /json/)
      .expect(200)
      .end((err, res) => {
        expect(res.body.platform).to.equal('foonation')
        done()
      })
  })

  it('Should return results with the right structure', done => {
    request(app)
      .get('/api/match')
      .query({ id: 20, platform: 'foonation' })
      .expect('Content-Type', /json/)
      .expect(200)
      .end((err, res) => {
        expect(res.body.platform).to.equal('foonation')
        expect(res.body).to.have.property('id')
        expect(res.body).to.have.property('datetime')
        expect(res.body).to.have.property('teams')

        expect(res.body.teams).to.have.property('local')
        expect(res.body.teams).to.have.property('away')

        done()
      })
  })

  it('Should return a 400 error if no platform is provided', done => {
    request(app)
      .get('/api/match')
      .query({ id: 20 })
      .expect('Content-Type', /json/)
      .expect(400, done)
  })

  it('Should return a 400 error if no id is provided', done => {
    request(app)
      .get('/api/match')
      .query({ platform: 'foonation' })
      .expect('Content-Type', /json/)
      .expect(400, done)
  })
})
