'use strict'

const router = require('express').Router()
const integrations = require('../integrations')

const match = async (request, response, next) => {
  const match_id = request.query.id
  const platform = request.query.platform

  const integration = integrations(platform)
  const result = await integration.getMatch(match_id)
  response.json(result)
}

router.get('/match', match)

module.exports = router
