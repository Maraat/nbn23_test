const integrations = {
  foonation: require('./foonation'),
  barbicle: require('./barbicle')
}
const get_module = platform => integrations[platform]

module.exports = get_module
