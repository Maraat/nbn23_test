const axios = require('axios')
const R = require('ramda')
const { DateTime } = require('luxon')

const getUrl = id => axios.get(`http://api.foonation.com/match?id=${id}`)
const getMatch = id => getUrl(id).then(response => parse(response.data))
const getTime = ([hour, minute]) => ({ hour, minute })
const getDatetime = match =>
  DateTime.fromMillis(match.date)
    .set(getTime(R.split(':', match.time)))
    .toString()

const parse = match => ({
  id: match.id,
  datetime: getDatetime(match),
  teams: {
    local: match.equipoA,
    away: match.equipoB
  },
  platform: 'foonation'
})

module.exports = { getMatch }
