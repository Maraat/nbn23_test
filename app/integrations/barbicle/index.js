const axios = require('axios')
const { DateTime } = require('luxon')

const getUrl = id => axios.get(`http://barbicle.com/api/Match/MatchInfo/${id}`)
const getMatch = id => getUrl(id).then(response => parse(response.data))

const parse = match => ({
  id: match.id,
  datetime: DateTime.fromISO(match.start).toString(),
  teams: {
    local: match.teams.local,
    away: match.teams.away
  },
  platform: 'barbicle'
})

module.exports = { getMatch }
