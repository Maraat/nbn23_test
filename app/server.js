'use strict'

const app = require('express')()
const morgan = require('morgan')

const validator = require('./middlewares').validator
const APIRouter = require('./routes/API.router')
if (process.env.NODE_ENV !== "production") require('./mocks')

app.use(morgan('tiny'))

app.use(validator)
app.use('/api', APIRouter)

app.listen(3000, () => console.log('API endpoint listening on port 3000'))

module.exports = app
