const nock = require('nock')
const foonation = nock('http://api.foonation.com')
  .persist()
  .get('/match')
  .query(true)
  .reply(200, {
    id: 1234,
    date: 1531313928957,
    time: '15:03',
    equipoA: 'L. A. Lakers',
    equipoB: 'Miami Heat'
  })

module.exports = foonation
