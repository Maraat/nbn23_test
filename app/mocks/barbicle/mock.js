const nock = require('nock')
const barbicle = nock('http://barbicle.com/api/Match/MatchInfo/')
  .persist()
  .get(/\d/)
  .reply(200, {
    id: 4567,
    start: '2018-07-11T13:03:29.859Z',
    teams: {
      local: 'L.A. Lakers',
      away: 'Miami Heat'
    }
  })

  module.exports = barbicle