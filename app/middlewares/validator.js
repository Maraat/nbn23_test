const handler = (request, response, next) => {
  const match_id = request.query.id
  if (!match_id) {
    response.status(400).send({
      text: 'You need to add a match id',
      field: 'id'
    })
    return
  }

  const platform = request.query.platform
  if (!platform) {
    response.status(400).send({
      text: 'You need to select a platform',
      field: 'platform'
    })
    return
  }

  next()
}

module.exports = handler
