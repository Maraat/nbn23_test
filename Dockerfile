FROM node:10-alpine
WORKDIR /usr/src/app
ENV NODE_ENV=develop
COPY package*.json ./
RUN npm install
EXPOSE 3000